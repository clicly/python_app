
from pathlib import Path
from termcolor import colored, cprint
from colorama import init, Fore, Back, Style
import re
import os
from leer_todos_html import read_html
from bs4 import BeautifulSoup
from slugify import slugify

ruta = './animales_amazonas/index.html'

def get_content(ruta, url ,nivel = 6):
	html = read_html(ruta)
	regex = re.compile(f'h[1-{nivel}]')
	soup = BeautifulSoup(html, 'html.parser')


	body = soup.find_all('body')
	
	cprint("="*50,'yellow', attrs = ['bold'])
	print("Ruta script:", os.getcwd())
	
	resultados = {}
	for index, b in enumerate(body):
		# Por cada nuevo bloque de contenido se 
		# crea una nueva lista de titulos
		titulos = []
		imagenes = b.find_all('img')
		# Se modifica el contenido
		for img in imagenes:
			ruta_img = './animales_amazonas' + img['src'][1:]
			path = Path(ruta_img)
			img['src'] = path
			print(path.is_file(), path)
			cprint("="*50,'yellow', attrs = ['bold'])
		
		for title in b.find_all(regex):
			# ~ cprint(title.get_text(), 'yellow', attrs = ['bold'])
			titulo = title.get_text()
			# convierte un string a un formato slug que es un string.
			slug = slugify(titulo)
			title['id'] = slug 
			# agrega una tupla slug_titulo a la lista de titulos en el html
			titulos.append((slug, titulo))
			
		# se convierte a formato de texto normal para el contenido
		resultados[f'body_{index}'] = {'contenido':b.decode_contents(), 
			'titulos':titulos}


			
		
	# ~ cprint("="*50, 'yellow', attrs = ['bold'])
	from pprint import pprint
	# ~ pprint(resultados,indent = 2)
		# ~ print("tabla:", type(b), b.find_all('table'))
	return resultados

# ~ get_content(ruta)

