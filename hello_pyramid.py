from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.view import view_config
from pprint import  pprint


class Persona:
	def __init__(self, nombre:str, apellido:str, edad:float):
		self.nombre = nombre
		self.apellido = apellido
		assert edad >= 0 , "No puede existir 'edad negativa menor que cero'"
		self.edad = edad

	# ~ Entrega una lista de datos formateados en un string
	def __str__(self):
		return f"{self.nombre} {self.apellido} {self.edad}"

	# ~ ENtrega una tupla para la clase Persona tambien formateada con string.
	def __repr__(self):
		return f"Persona({self.nombre}, {self.apellido}, {self.edad})"

@view_config(
	route_name='home',
	renderer='template_pyramid/home.jinja2'
)
def home(request):
	nombres = ["Juan", "Nelson", "Pedro", 
		"Julián", "Gabriel"]
	apellidos = ["Pérez", "Villavicencio", "Del Valle", "Asturias", 
		"Sahir"]
	edades = [25, 32, 33, 27, 28]
	info_personas = {posicion:Persona(*datos) for posicion,datos in 
		enumerate(zip(nombres, apellidos, edades))}
	pprint(info_personas, indent = 2)
	
	# Utilizando función zip para comprimir
	# las dos listas 'nombres' 'edades'
	# luego mostrarlas
	personas = list(zip(nombres,edades))
	
	return {"greet": 'Bienvenida', "name": 'Raquel', 
		"bloque":'Esto es una prueba',
		"personas":personas, 
		"titulo_lista":"Datos",
		"informaciones":info_personas
		}
	
if __name__ == '__main__':
	with Configurator() as config:
		config.include('pyramid_jinja2')
		config.add_route('home', '/')
		config.scan()
		app = config.make_wsgi_app()
	server = make_server('0.0.0.0', 6543, app)
	server.serve_forever()
