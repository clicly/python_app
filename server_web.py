from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.view import view_config
from pprint import  pprint
from contenido_html import get_content


@view_config(
	route_name = 'home',
	renderer = './templates/articulo.jinja2'
)
def server_web(request):
	ruta = './animales_amazonas/index.html'
	data = get_content(ruta)
	pprint("*"*60)
	pprint(data.get('body_0').keys(), indent = 2)
	pprint("*"*60)
	return {'contenido': data, 
		'titulo': 'Animales en Amazonas'}


if __name__ == '__main__':
	with Configurator() as config:
		config.include('pyramid_jinja2')
		config.add_route('home', '/')
		config.scan()
		app = config.make_wsgi_app()
	server = make_server('0.0.0.0', 6543, app)
	server.serve_forever()
