from pathlib import Path
# ~ import os
# ~ import re
# ~ from jinja2 import FileSystemLoader, Environment, select_autoescape
def read_html(ruta):
	file_html = Path(ruta)
	# ~ print("¿Hay una ruta? -> ",file_html.is_file())
	modo = 'r'
	if file_html.is_file():
		with open(ruta, modo) as archivo:
			# ~ Lectura del archivo 
			texto_html = archivo.read()
		return texto_html
