#!/bin/usr/python3

from pathlib import Path
import os
import re
from termcolor import colored, cprint
from colorama import init, Fore, Back, Style
import time 
import tarfile
import zipfile
import rarfile
import py7zr
from nombre_archivo import nombre_archivo


def ruta_html(ruta):
	'''
	> ruta: (parametro en la funcion) recibe la ruta 
	luego de que for itere
	sobre la variable 'lista_rutas_archivos' 
	> file_list, guardará el resultado cuando
	archiv_iter revisa si el elemento es un archivo,
	esto es para obtener los nombres de las rutas que
	coincidan con un patrón; en este caso '/' 
	if, si archivo_iter concede una ruta que coincida 
	con este patrón el metodo is_file() >de Path<
	devolverá la ruta abajo en el script.
	'''
	# ~ agregar la ruta a la variable Path
	path = Path(ruta)
	file_list = [archivo_iter for archivo_iter in 
		path.glob('**/*') if archivo_iter.is_file()]
	return file_list
	

if __name__ == "__main__":
	# ~ lista temporal de directorios
	lista_rutas_archivos = ['./destino_historial', 
		'./destino_otro', './destino_samp']
	'''
	la variable ruta itera sobre la lista_rutas_archivos
	recuperando el 'resultado' que será el llamado a la
	función ruta_hmtl(ruta)
	luego imprime la ruta del directorio
	'''
	for ruta in lista_rutas_archivos:
		resultado = ruta_html(ruta)
		print(f"Para {ruta} es:\n {resultado}")
		time.sleep(0.3)
		print()
	# ~ ruta = "./origen"
	# ~ resultado = ruta_html(ruta)
	# ~ print(resultado)

