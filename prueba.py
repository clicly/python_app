from pathlib import Path
import os
import re

from termcolor import colored, cprint
from colorama import init, Fore, Back, Style
import time 
import tarfile
import zipfile
import rarfile
import py7zr
from nombre_archivo import nombre_archivo

'''
Extraer metadata de los archivos 
'''
def getMetadata(path):
	print()
	cprint(f"Mostrando metadata de: '{path}'", 'yellow', attrs = ['bold'])
	stats = os.stat(path)
	
	texto1 = colored("size: ", 'green', attrs = ['bold'])
	print(texto1, stats.st_size / 1024, "kilobytes")
	
	# ~ for stat in stats:
		# ~ print(stat)


def getMetodo(path):
	'''
	Diccionario con valor y llave, cada valor representa el nombre de las
	funciones definidas
	'''
	metodos = {
	"zip":unzip, "tar":untar, "rar":unrar, "7z":un7zip
		}
	
	'''
	Lista con las extensiones a aceptar en el script, incluyendo una expresión 
	regular [gx], un tipo de extensión.
	'''
	extensiones = [("zip", r"\.zip$"), 
					("tar", r"\.tar\.[gx]z$"), 
					("rar", r"\.rar$"), 
					("7z" ,r"\.7z")]
	
	'''
	'''
	regex_extensiones = {key : re.compile(regex) for key,regex in 
		extensiones}
	print(regex_extensiones)


	for key, regex in regex_extensiones.items():
		print(regex)
		
		if regex.search(str(path)):
			return metodos.get(key,print) 
			 


def unzip(ruta_zip, destino = "./destino"):
	'''
ZIP
parametros:
> ruta_zip: ocupa la ruta del origen del o los archivos comrpimidos
en la carpeta 'origen'
> destino: es donde se descomprimirá el o los archivos, 'destino'
se anexará al nombre del archivo cuando sea descomprimido 
generando una carpeta 'origen_directorio' individual
> estructura if: 
 - zipfile.is_zipfile: verificar si el archivo que está en 'ruta_zip'
 es valido para esta extension entonces:
 > with: creador de contexto, abrirá y leerá 'modo = 'r' (read)
 el archivo, lo extraerá o descomprimira en la ruta 'destino'.
 retornar que el proceso fue con valor True o verdadero
 > else: sino entregar que no se cumplió, por lo tanto es False.
'''
	modo = 'r'
	if zipfile.is_zipfile(ruta_zip):
		with zipfile.ZipFile(ruta_zip, modo) as ruta_zip:
			ruta_zip.extractall(destino)
			return True
	else:
		return False
	
'''
TAR
'''
def untar(ruta_tar, destino = "./destino"):
	modo = 'r'
	if tarfile.is_tarfile(ruta_tar):
		with tarfile.open(ruta_tar, modo) as ruta_tar:
			ruta_tar.extractall(destino)
			return True
	else:
		return False
	
''' 
RAR 
'''
def unrar(ruta_rar, destino = "./destino"):
	modo = 'r'
	if rarfile.is_rarfile(ruta_rar):
		with rarfile.RarFile(ruta_rar, modo) as ruta_rar:
			ruta_rar.extractall(destino)
			return True
	else:
		return False
	
'''
7zip
'''
def un7zip(ruta_7z, destino = "./destino"):
	modo = 'r'
	if py7zr.is_7zfile(ruta_7z):
		with py7zr.SevenZipFile(ruta_7z, modo) as ruta_7z:
			ruta_7z.extractall(destino)
			return True
	else:
		return False


def ruta_html(ruta):
	'''
	> ruta: (parametro en la funcion) recibe la ruta 
	luego de que for itere
	sobre la variable 'lista_rutas_archivos' 
	> file_list, guardará el resultado cuando
	archiv_iter revisa si el elemento es un archivo,
	esto es para obtener los nombres de las rutas que
	coincidan con un patrón; en este caso '/' 
	if, si archivo_iter concede una ruta que coincida 
	con este patrón el metodo is_file() >de Path<
	devolverá la ruta abajo en el script.
	'''
	# ~ agregar la ruta a la variable Path
	path = Path(ruta)
	file_list = [archivo_iter for archivo_iter in 
		path.glob('**/*') if archivo_iter.is_file()]
	return file_list


if __name__ == "__main__":

	archivos = ['./origen/archivo.zip',
		'./origen/curso-python-para-principiantes.tar.xz', 
		'./origen/archivo.tar.gz', 'hola.zip', 'prueba.tar.xz',
		'./origen/historial.tar.gz', './origen/push.zip', './origen/otro.rar',
		'./origen/samp.7z']
	
	lista_rutas_archivos = ['./destino_historial', 
		'./destino_otro', './destino_samp']
		
	'''
	la variable ruta itera sobre la lista_rutas_archivos
	recuperando el 'resultado' que será el llamado a la
	función ruta_hmtl(ruta)
	luego imprime la ruta del directorio
	'''
	for ruta in lista_rutas_archivos:
		resultado = ruta_html(ruta)
		print(f"Para {ruta} es:\n {resultado}")
		time.sleep(0.3)
		print()
	
	for archivo in archivos:
		path = Path(archivo)
		nombre = nombre_archivo(archivo)
		destino = f"./destino_{nombre}"
		if path.exists():
			# ~ print(archivo, type(archivo), path, type(path))
			getMetadata(path)
			metodo = getMetodo(path)
			resultado = metodo(path, destino = destino)
			cprint(f"El metodo para descomprimir es: {metodo}", 'red', 
				attrs = ['bold'])
		else: 
			texto2 = colored("No existe: ", 'green','on_cyan', 
				attrs = ['bold'])
			cprint(f"{texto2} {archivo}")
			
	
