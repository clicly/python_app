from pathlib import Path
import os
import re

from termcolor import colored, cprint
from colorama import init, Fore, Back, Style
import time 

import tarfile
from nombre_archivo import nombre_archivo

'''
Extraer metadata de los archivos 
'''

def getMetadata(path):
	print()
	cprint(f"Mostrando metadata de: '{path}'", 'yellow', attrs = ['bold'])
	stats = os.stat(path)
	
	texto1 = colored("size: ", 'green', attrs = ['bold'])
	print(texto1, stats.st_size / 1024, "kilobytes")
	
	# ~ for stat in stats:
		# ~ print(stat)


def getMetodo(path):
	
	metodos = {
	"zip":unzip, "tar":untar, "rar":unrar, "7z":un7zip,
		}
	
	extensiones = [("zip", r"\.zip$"), 
					("tar", r"\.tar\.[gx]z$"), 
					("rar", r"\.rar$"), 
					("7z" ,r"\.7z")]
		
	regex_extensiones = {key : re.compile(regex) for key,regex in 
		extensiones}
	print(regex_extensiones)


	for key, regex in regex_extensiones.items():
		print(regex)
		
		if regex.search(str(path)):
			return metodos.get(key,print) 
			 


def unzip(ruta_unzip, destino = "./destino"):
	
	
def untar(ruta_tar, destino = "./destino"):
	# ~ ruta_tar = "./origen/ejemplo.tar.gz"
	modo = 'r'
	if tarfile.is_tarfile(ruta_tar):
		with tarfile.open(ruta_tar, modo) as ruta_tar:
			ruta_tar.extractall(destino)
			return True
	else:
		return False
	
	
def unrar(ruta_unrar, destino = "./destino"):
	return
	
	
def un7zip(ruta_un7zip, destino = "./destino"):
	return

if __name__ == "__main__":
	

	archivos = ['./origen/archivo.zip',
		'./origen/curso-python-para-principiantes.tar.xz', 
		'./origen/archivo.tar.gz', './origen/hola.zip', 
		'./origen/prueba.tar.xz', './origen/historial.tar.gz',
		'./origen/other.zip', './origen/push.zip']
	
	# ~ archivos = ['./origen/other.zip']
	
	for archivo in archivos:
		# ~ print(archivo)
		path = Path(archivo)
		nombre = nombre_archivo(archivo)
		destino = f"./destino_{nombre}"
		if path.exists():
			print(archivo, type(archivo), path, type(path))
			getMetadata(path)
			metodo = getMetodo(path)
			resultado = metodo(path, destino = destino) 
			cprint(f"El metodo para descomprimir es: {metodo}", 'red', 
				attrs = ['bold'])
			print(f"El resultado de descomprimir {archivo} es {resultado}")
		else: 
			texto2 = colored("No existe: ", 'green','on_cyan', 
				attrs = ['bold'])
			cprint(f"{texto2} {archivo}")
			
	
