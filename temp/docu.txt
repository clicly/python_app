7z
https://pypi.org/project/py7zr/
https://py7zr.readthedocs.io/en/latest/user_guide.html
https://py7zr.readthedocs.io/en/latest/api.html

zipfile
https://docs.python.org/3/library/zipfile.html?highlight=zip#module-zipfile


rarfile
https://pypi.org/project/rarfile/
https://python-unrar.readthedocs.io/en/v0.3/rarfile.html#rarfile-objects
https://python-unrar.readthedocs.io/_/downloads/en/latest/pdf/

tarfile
https://docs.python.org/3/library/tarfile.html

os.path
https://docs.python.org/3/library/os.path.html?highlight=path#module-os.path
