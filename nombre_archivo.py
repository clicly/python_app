#!/bin/usr/python3

def nombre_archivo(ruta_archivo):
	'''
	Parametro de entrada: ruta_archivo
	string que hace referencia a la ruta
	del archivo seleccionado y luego se
	asigna metodo split para separar
	el nombre según '/' que define la ruta,
	'''
	lista = ruta_archivo.split("/")
	nombre = lista[-1]
	lista_nombre = nombre.split(".")
	return lista_nombre[0]

