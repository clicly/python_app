from pathlib import Path
import os
import re

from termcolor import colored, cprint
from colorama import init, Fore, Back, Style
import time 
import tarfile
import zipfile
import rarfile
import py7zr
from nombre_archivo import nombre_archivo
from html.parser import HTMLParser
from jinja2 import FileSystemLoader, Environment, select_autoescape


# ~ from busqueda_parser import Titulo

from contenido_html import get_content

'''
Este script tiene como función leer la estructura interna de un archivo
HTML y obtener los elementos <title>, luego asignarlos a una lista creando
un nuevo archivo con extensión csv 
'''
# ~ ruta = r"./animales_amazonas/index.html"

# ~ ruta al archivo
ruta = "./animales_amazonas/index.html"

# ~ inicializar clase parser que está ṕor defecto para analizar el texto
# ~ de los archivos html, esto junto con la clase creada 'Titulo' definiendo
# ~ a la variable clases, el texto que se encuentre entre las etiquetas div
# ~ cuya función sea clases para titulos.
# ~ parser = Titulo(n = 5, clases = ["title"])

# archivo html 
# ~ file_html = Path(ruta)
# ~ print("¿Hay una ruta? -> ",file_html.is_file())

# ~ modo = 'r'
# ~ if file_html.is_file():
	# ~ with open(ruta, modo) as archivo:
		#Lectura del archivo 
		# ~ texto_html = archivo.read()
		# ~ parser.feed(texto_html)
		# ~ titulos = parser.titulos
		# ~ contenidos = parser.contenidos
# ~ else:
	# ~ print(False)

content = get_content(ruta)
print("content:",type(content))
# ~ Leer en contexto el archivo html como texto

'''
Siendo importado FileSystem y los otros atributos de jinja2
se requiere leer el archivo como texto
'''
# Crear un ambiente/entorno para la plantilla que se indica, esto con la
#configuración predeterminada de FileSystemLoader
env = Environment(loader=FileSystemLoader('./templates'), 
	autoescape=select_autoescape('html'))

#get_template (metodo) se utilizará para cargar la plantilla y que la devuelva
#luego como texto
template = env.get_template('articulo.html')
final = template.render(contenido = content)
print("Esto es contenidos - Variable: ", content)
# ~ print(final)

with open('resultado_web.html', 'w') as archivo:
	archivo.write(final)
	
	

